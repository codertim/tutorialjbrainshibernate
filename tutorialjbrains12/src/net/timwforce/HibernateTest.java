package net.timwforce;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import net.timwforce.dto.Address;
import net.timwforce.dto.UserStuff;

public class HibernateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		UserStuff user = new UserStuff();
		
		Address addr = new Address();
		addr.setCity("Ventura");
		addr.setStreet("555 Main St.");
		addr.setPincode("123");
		// user.setHomeAddress(addr);
		
		Address addr2 = new Address();
		addr2.setCity("Oxnard");
		addr2.setStreet("321 Oxnard Blvd.");
		addr2.setPincode("555");
		// user.setOfficeAddress(addr2);
		
		user.getListOfAddresses().add(addr);
		user.getListOfAddresses().add(addr2);
		
		// user.setUserId(7);  use GeneratedValue annotation instead
		user.setUsername("User 7");
		user.setJoinedDate(new Date());
		user.setDescription("Description goes here");
		
		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
		session.close();
		
		
		session = sessionFactory.openSession();
		UserStuff user2 = (UserStuff) session.get(UserStuff.class, 10);
		System.out.println("Username retrieved is: " + user2.getUsername());
	}

}
