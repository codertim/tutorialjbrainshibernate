package net.timwforce.dto;

import javax.persistence.Embeddable;


import javax.persistence.Column;


@Embeddable
public class Address {
	@Column (name="CITY_NAME")
	private String city;
	private String state;
	private String street;
	private String pincode;
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;

	}
	public void setState(String state) {
		this.state = state;
	}
	public String getStreet() {
		return street;
	}
	public void setStreet(String street) {
		this.street = street;
	}
	public String getPinCode() {
		return pincode;
	}
	public void setPincode(String pinCode) {
		this.pincode = pinCode;
	}
	
	
}
