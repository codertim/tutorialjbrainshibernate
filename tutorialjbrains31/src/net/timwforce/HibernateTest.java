package net.timwforce;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;

import net.timwforce.dto.Address;
import net.timwforce.dto.UserStuff;
import net.timwforce.dto.Vehicle;

public class HibernateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		UserStuff user = new UserStuff();
		
		
		// user.setUserId(7);  use GeneratedValue annotation instead
		user.setUsername("User 100");

		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		// session.persist(user);
		//  session.save(user);
	    // user.setUsername("User 101");
		// user = (UserStuff) session.get(UserStuff.class, 10);
		// System.out.println ("user = " + user);
		
		String minUserId = "5";
		// Query query = session.createQuery("select username from UserStuff where userId > ?");
		Query query = session.createQuery("select username from UserStuff where userId > :user_id");
		// query.setInteger(0, Integer.parseInt(minUserId));   // ep. 27
		query.setInteger("user_id", Integer.parseInt(minUserId));   // ep. 27
		query.setFirstResult(2);   // ep. 26 - set x number of records
		query.setMaxResults(2);     // ep. 26
		// List<UserStuff> userList = (List<UserStuff>) query.list();
		List<String> userList = (List<String>) query.list();
		System.out.println("Size of list: " + userList.size());		
		for (String u : userList) {
			System.out.println(u);
		}
		
		// ep. 26 - max id
		Query query2 = session.createQuery("select max(userId) from UserStuff where userId > 5");
		List<Integer> maxUserIds = (List<Integer>) query2.list();
		for (Integer i : maxUserIds) {
			System.out.println("max user id: " + i);
		}
		 
		// ep. 28 - named query
		Query query3 = session.getNamedQuery("UserStuff.byId");
		query3.setInteger(0, 2);
		
		// Query query4 = session.getNamedQuery("UserStuff.byName");
		// query4.setString(0, "some name");
		
		// ep. 29 - criteria
		Criteria criteria = session.createCriteria(UserStuff.class);
		criteria.add(Restrictions.eq("username", "User 101"))
		        .add(Restrictions.gt("user_id", 2))
		        .add(Restrictions.like("usernae", "%101"))
		        .add(Restrictions.between("user_id", 1, 200));
		List<UserStuff> usersFromCriteria = (List<UserStuff>) criteria.list();
		for(UserStuff u : usersFromCriteria) {
			System.out.println("*** Criteria user: " + u);
		}
		// ep. 30 - or clause
		// criteria.or(Restrictions.eq("username", "User 101"), Restrictions.between("user_id", 1, 200));

		// ep. 31 - projections
		Criteria criteria2 = session.createCriteria(UserStuff.class)
				             .setProjection(Projections.property("userId"));
				             // .addOrder(Order.desc("userId"));
		
		
		
		session.getTransaction().commit();
		session.close();
		
		session = sessionFactory.openSession();
		UserStuff user2 = (UserStuff) session.get(UserStuff.class, 10);
		System.out.println("Username retrieved is: " + user2.getUsername());
	}

}
