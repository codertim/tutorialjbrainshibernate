package net.timwforce.dto;

import java.util.ArrayList;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Vehicle {
	@Id @GeneratedValue
	private int vehicleId;
	private String vehicleName;
	// @ManyToOne   // tutorials 14 and 15(mappedBy)
	// @JoinColumn(name="USER_ID")  // for tutorial 15(mappedBy)

	// private UserStuff user;
	/* pre-16
	@ManyToMany(mappedBy="vehicle")
	private Collection<UserStuff> userList = new ArrayList();
	public Collection<UserStuff> getUserList() {
		return userList;
	}
	public void setUserList(Collection<UserStuff> userList) {
		this.userList = userList;
	}
	*/
	
	
	/*
	public UserStuff getUser() {
		return user;
	}
	public void setUser(UserStuff user) {
		this.user = user;
	}
	*/
	public String getVehicleName() {
		return vehicleName;
	}
	public void setVehicleName(String vehicleName) {
		this.vehicleName = vehicleName;
	}
	public int getVehicleId() {
		return vehicleId;
	}
	public void setVehicleId(int vehicleId) {
		this.vehicleId = vehicleId;
	}
	
	
}
