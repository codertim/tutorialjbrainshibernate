package net.timwforce;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import net.timwforce.dto.Address;
import net.timwforce.dto.UserStuff;
import net.timwforce.dto.Vehicle;

public class HibernateTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		UserStuff user = new UserStuff();
		
		
		// user.setUserId(7);  use GeneratedValue annotation instead
		user.setUsername("User 7");

		Vehicle vehicle = new Vehicle();
		vehicle.setVehicleName("Car");
		Vehicle vehicle2 = new Vehicle();
		vehicle2.setVehicleName("Jeep");
		
		// user.setVehicle(vehicle);
		user.getVehicle().add(vehicle);
		user.getVehicle().add(vehicle2);
		// vehicle.getUserList().add(user);  15
		// vehicle2.getUserList().add(user);  15
		// vehicle.setUser(user);   // ManyToOne
		// vehicle2.setUser(user);  // ManyToOne

		SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.persist(user);
		// session.save(user);
		// session.save(vehicle);
		// session.save(vehicle2);
		session.getTransaction().commit();
		session.close();
		
		
		session = sessionFactory.openSession();
		UserStuff user2 = (UserStuff) session.get(UserStuff.class, 10);
		System.out.println("Username retrieved is: " + user2.getUsername());
	}

}
